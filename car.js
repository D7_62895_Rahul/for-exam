const express=require('express')
const mysql=require('mysql')
const db=require('./db')

const router=express.Router()

router.post('/car',(request,response)=>{
    const{id, name, company, prize}=request.body

    const statement=`Insert into  Car values(?,?,?,?)`
 db.pool.query(statement,[id, name, company, prize],(error,result)=>{
     if(error)
     {
         response.send(error)

     }
     else{
         response.send(result)
     }
 })

})

router.get('/',(request,response)=>{
    

    const statement=`Select * from Car `
 db.pool.query(statement,(error,result)=>{
     if(error)
     {
         response.send(error)

     }
     else{
         response.send(result)
     }
 })

})

router.put('/',(request,response)=>{
    const{id, name, company, prize}=request.body

    const statement=`update  Car set car_id=?, car_name=?, company_name=?, car_prize=?`
 db.pool.query(statement,[id, name, company, prize],(error,result)=>{
     if(error)
     {
         response.send(error)

     }
     else{
         response.send(result)
     }
 })

})
router.delete('/:id',(request,response)=>{
    
   const {id}=request.params
    const statement=`delete  from Car where car_id=?`
 db.pool.query(statement,[id],(error,result)=>{
     if(error)
     {
         response.send(error)

     }
     else{
         response.send(result)
     }
 })

})
module.exports=router;